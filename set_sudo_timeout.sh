#!/bin/bash

# Check if the script is running as root
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root"
    exit 1
fi

# Backup the sudoers file
cp /etc/sudoers /etc/sudoers.bak

# Check if timestamp_timeout is already set
grep -q "Defaults timestamp_timeout=" /etc/sudoers

# If it is, replace it with the desired value
if [ $? -eq 0 ]; then
    sed -i 's/Defaults timestamp_timeout=[0-9]*$/Defaults timestamp_timeout=60/' /etc/sudoers
else
    # If it isn't, add it after "Defaults env_reset"
    sed -i '/Defaults env_reset/a Defaults timestamp_timeout=60' /etc/sudoers
fi

echo "timestamp_timeout set to 60 minutes in sudoers file."

