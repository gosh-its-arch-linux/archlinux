#!/bin/bash

# Check if the script is running as root
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root"
    exit 1
fi

# Check if we have a backup
if [[ ! -f /etc/sudoers.bak ]]; then
    echo "Backup not found. Cannot revert."
    exit 1
fi

# Replace the sudoers file with the backup
cp /etc/sudoers.bak /etc/sudoers

echo "Reverted the sudoers file to its original state."


