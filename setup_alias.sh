#!/bin/bash

echo "Welcome to Gosh-Its-Arch Alias updater"

# Define the aliases and their descriptions
declare -A aliases
aliases["ls"]="alias ls='exa -al --color=always --group-directories-first'"
aliases["la"]="alias la='exa -a --color=always --group-directories-first'"
aliases["ll"]="alias ll='exa -l --color=always --group-directories-first'"
aliases["lt"]="alias lt='exa -aT --color=always --group-directories-first'"
aliases["df"]="alias df='duf'"
aliases["cat"]="alias cat='bat'"
aliases["top"]="alias top='htop'"
aliases["colorscript"]="colorscript random"
aliases["neofetch"]="neofetch"
aliases["misfortune"]="misfortune"

# Option to add all at the beginning
read -p "Do you want to add all aliases at once? (y/n) " allResponse

if [[ $allResponse =~ ^[Yy]$ ]]; then
    for cmd in "${aliases[@]}"; do
        echo "$cmd" >> ~/.bashrc
    done
    echo "All aliases added. Please run 'source ~/.bashrc' to apply changes."
    exit 0
fi

# Loop over each alias, prompting the user
for cmd in "${!aliases[@]}"; do
    read -p "Do you want to add $cmd alias? (y/n) " response
    if [[ $response =~ ^[Yy]$ ]]; then
        echo "${aliases[$cmd]}" >> ~/.bashrc
    fi
done

echo "Done. Please run 'source ~/.bashrc' to apply changes."

