#!/bin/bash

echo "Interactive Rclone Config Updater"

# Check if rclone.conf exists in the current directory
if [[ ! -f rclone.conf ]]; then
    echo "Error: 'rclone.conf' does not exist in the current directory."
    exit 1
fi

# Confirm with the user
read -p "Are you sure you want to append 'rclone.conf' to '/home/$USER/.config/rclone/rclone.conf'? (y/n) " response

if [[ $response =~ ^[Yy]$ ]]; then
    # Ensure the destination directory exists
    mkdir -p /home/$USER/.config/rclone

    # Append the file
    cat rclone.conf >> /home/$USER/.config/rclone/rclone.conf

    echo "Operation completed successfully."
else
    echo "Operation cancelled."
fi

