#!/bin/bash

# Source file
SRC="nextcloud.cfg"

# Destination directory
DEST_DIR="/home/$USER/.config/Nextcloud"

# Ensure the destination directory exists
if [[ ! -d $DEST_DIR ]]; then
    echo "Creating $DEST_DIR ..."
    mkdir -p $DEST_DIR
fi

# Copy the file
if [[ -f $SRC ]]; then
    cp $SRC $DEST_DIR
    echo "$SRC copied to $DEST_DIR"
else
    echo "Error: $SRC does not exist!"
    exit 1
fi

# Change ownership to gosh
chown gosh $DEST_DIR/$SRC
echo "Ownership of $DEST_DIR/$SRC set to gosh"

